# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2020-10-01

### Removed

- Removed section acquiring a digital certificate from the README file.


## [1.0.0] - 2020-09-28

### Added

- Examples of timestamp issue. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/carimbo-autenticacao-basica/-/tags/1.0.0
[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/java/carimbo-autenticacao-basica/-/tags/1.0.1
