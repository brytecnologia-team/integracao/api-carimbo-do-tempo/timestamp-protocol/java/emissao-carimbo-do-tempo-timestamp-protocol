package br.com.bry.framework.exemplo.config;

public class ServiceConfig {

	public static final String URL_TIMESTAMP_SERVER = "https://cloud.bry.com.br/carimbo-do-tempo/tsp";

	public static final String CLIENT_ID = "Client id da credencial";

	public static final String CLIENT_SECRET = "Client secret da credencial";

}
