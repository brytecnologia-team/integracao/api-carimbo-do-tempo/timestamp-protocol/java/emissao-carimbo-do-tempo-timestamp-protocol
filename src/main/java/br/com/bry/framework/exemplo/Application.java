package br.com.bry.framework.exemplo;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.security.MessageDigest;
import java.security.SecureRandom;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.SignerId;
import org.bouncycastle.tsp.TimeStampRequest;
import org.bouncycastle.tsp.TimeStampRequestGenerator;
import org.bouncycastle.tsp.TimeStampResponse;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.tsp.TimeStampTokenInfo;
import org.bouncycastle.util.encoders.Base64;

import br.com.bry.framework.exemplo.config.ServiceConfig;

public class Application {

	private static final String TIMESTAMP_QUERY_CONTENT_TYPE = "application/timestamp-query";
	private static String CLIENT_ID = ServiceConfig.CLIENT_ID;
	private static String CLIENT_SECRET = ServiceConfig.CLIENT_SECRET;
	private static String URL = ServiceConfig.URL_TIMESTAMP_SERVER;

	public static void main(String[] args) {

		try {

			String basicAuthCredentials = CLIENT_ID + ":" + CLIENT_SECRET;
			String encoding = new String(Base64.encode(basicAuthCredentials.getBytes()));

			// Calculando o resumo criptografico SHA-256 do conteudo que sera carimbado
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] digest = messageDigest.digest("Teste".getBytes());

			TimeStampRequestGenerator timeStampRequestGenerator = new TimeStampRequestGenerator();
			timeStampRequestGenerator.setCertReq(true);
			// Definido o OID da politica
			timeStampRequestGenerator.setReqPolicy("2.16.76.1.6.6");

			// OID SHA-256
			TimeStampRequest timeStampRequest = timeStampRequestGenerator.generate("2.16.840.1.101.3.4.2.1", digest,
					BigInteger.valueOf(new SecureRandom().nextInt()));

			java.net.URL url = new java.net.URL(URL);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			connection.setRequestProperty("Content-Type", TIMESTAMP_QUERY_CONTENT_TYPE);

			// Enviando requisição de carimbo do tempo
			OutputStream connectionOutputStream = connection.getOutputStream();
			connectionOutputStream.write(timeStampRequest.getEncoded());
			connectionOutputStream.flush();

			InputStream inputStream = connection.getInputStream();
			byte[] response = IOUtils.toByteArray(inputStream);
			inputStream.close();

			// Tratando a resposta
			TimeStampResponse timeStampResponse = new TimeStampResponse(response);
			timeStampResponse.validate(timeStampRequest);
			System.out.println("Carimbo do tempo verificado.");
			TimeStampToken tsToken = timeStampResponse.getTimeStampToken();
			TimeStampTokenInfo tsInfo = tsToken.getTimeStampInfo();

			SignerId signer_id = tsToken.getSID();
			System.out.println("Generation time " + tsInfo.getGenTime());
			System.out.println("Signer ID serial " + signer_id.getSerialNumber());
			System.out.println("Signer ID issuer " + signer_id.getIssuer());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
