# Emissão de Carimbo do Tempo com Autenticação Básica

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java para emissão de carimbo do tempo utilizando autenticação básica e requisição conforme especificação da RFC 3161. 

### Tech

O exemplo utiliza as bibliotecas Java abaixo:
* [Apache Commons IO] - The Apache Commons IO library contains utility classes, stream implementations, file filters, file comparators, endian transformation classes, and much more
* [Bouncy Castle] - The Bouncy Castle Java APIs for CMS, PKCS, EAC, TSP, CMP, CRMF, OCSP, and certificate generation.
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com uma credencial (login/senha) válida.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à emissão do carimbo do tempo.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| CLIENT_ID | Client id definido na credencial. | ServiceConfig
| CLIENT_SECRET | Client secret definido na credencial | ServiceConfig

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência (utilizamos o Eclipse).

Executar programa:

	clique com o botão direito em cima do arquivo Application.java -> Run as -> Java Application

   [Apache Commons IO]: <https://mvnrepository.com/artifact/commons-io/commons-io>
   [Bouncy Castle]: <https://mvnrepository.com/artifact/org.bouncycastle/bcpkix-jdk15on>
   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
